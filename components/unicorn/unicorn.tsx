import * as React from "react";
import { StyleRules, Theme, makeStyles} from "@material-ui/core";

interface OwnProps{
    
}

const useStyles = makeStyles(
    (theme: Theme): StyleRules => ({
        root: {
            color: theme.palette.primary.main
        }
    })
);


export const UniCorn: React.FC<OwnProps> = (props) => {
    const classes = useStyles(props);

    return (
        <React.Fragment>
            <div className={classes.root}>
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUcaS8XF3br7Arvsnz65BSi-czZKtgwuip9A&usqp=CAU" alt="UniCorn"/>
            </div>
        </React.Fragment>
    );

};
import React from "react";
import { makeStyles } from "@material-ui/core";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import { useScroll } from "../../../logic/hooks/use-scoll";

const useStyles = makeStyles((theme: any) => ({
  root: (props: OwnProps) => ({
    backgroundColor: theme.palette.secondary.main,
    color: "#fff",
    cursor: "pointer",
    width: 32,
    height: 32,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 2,
    position: "fixed",
    margin: theme.spacing(3),
    right: props.position === "right" ? 0 : "unset",
    left: props.position === "left" ? 0 : "unset",
    bottom: theme.spacing(60),
    // boxShadow: "3px 3px rgba(0,0,0,0.2)",
    "&::before": {
      content: "'" + props.text + "'",
      display: props.text ? "block" : "none",
      position: "absolute",
      right: props.position === "right" ? 40 : "unset",
      left: props.position === "left" ? 40 : "unset",
      width: "100px",
      textAlign: props.position === "right" ? "right" : "left",
      top: 6,
      fontSize: ".9em",
      opacity: 0,
      transition: "all .45s",
      color: theme.palette.secondary.main,
    },
    "&:hover::before": {
      opacity: 1,
    },
    "&:hover svg": {
      cursor: "pointer",
      transform: "scale(1.2) translateY(-5px)",
    },
  }),
  icon: {
    transition: ".3s all",
  },
}));

interface OwnProps {
  position?: "right" | "left";
  text?: string;
}

export const ScrollToTop: React.FC<OwnProps> = (props: OwnProps) => {
  const scrolled = useScroll();
  const classes = useStyles(props);

  return scrolled ? (
    <div
      className={classes.root}
      onClick={() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }}
    >
      <ArrowUpwardIcon className={classes.icon} />
    </div>
  ) : (
    <></>
  );
};

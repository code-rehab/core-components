const express = require('express')
const app = express()
const port = 3050
const path = require('path');

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/wizard/welcome.html'));
})

app.get('/run-app',(req,res) =>{

  const { exec } = require("child_process");

  exec(
    `cd ../app && yarn start`,
    (err, stdout, stderr) => {
      if (err) {
        console.error(`${err}`);
      }

      if (stderr) {
        console.error(`${stderr}`);
      }

      if (stdout) {
        res.send('Material UI is beeing installed')
        console.log(`${stdout}`);
      }
    }
  );
})

app.get('/core-install', (req, res) => {
 
  const { exec } = require("child_process");

  exec(
    `wp core download --path="../wp"`,
    (err, stdout, stderr) => {
      if (err) {
        console.error(`${err}`);
      }

      if (stderr) {
        console.error(`${stderr}`);
      }

      if (stdout) {
        res.send('WP CORE is beeing installed')
        console.log(`${stdout}`);
      }
    }
  );
})

app.get('/app-install', (req, res) => {
 
  const { exec } = require("child_process");

  exec(
    `npx create-react-app ../app --template=typescript`,
    (err, stdout, stderr) => {
      if (err) {
        console.error(`${err}`);
      }

      if (stderr) {
        console.error(`${stderr}`);
      }

      if (stdout) {
        exec(
          `cd ../app && yarn add @material-ui/core && yarn add @material-ui/icons `,
          (err, stdout, stderr) => {
            if (err) {
              console.error(`${err}`);
            }
      
            if (stderr) {
              console.error(`${stderr}`);
            }
      
            if (stdout) {
              res.send('Material UI is beeing installed')
              console.log(`${stdout}`);
            }
          }
        );
        console.log(`${stdout}`);
      }
    }
  );

 
})

app.listen(port, () => {
  console.log(`Wizard running at http://localhost:${port}`)
})